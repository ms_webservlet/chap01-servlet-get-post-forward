package com.mseop.forwrad;

public class Application {
	/*
	 * 
	 * chap01-Servlet -life-cycle
		init : 서블릿이 요청이 최초인 경우 한번만 동작하는 메소드
		destroy : 컨테이너가 종료될 때 호출되는 메소드, 주로 자원 반납하는 용도로 사용.
		
		HttpServletRequest 
		
		HttpRequest [요청을받는거]
		view페이지에서 서블릿을 요청을 딱 보내면 
		서블릿을 실행한거,실행할때 필요한정보를 가지고있다. 
		http모든 프로토콜을 가지고있는상태다.
		HttpServletRequest
		GET 조회 할때
		POST 인설트 할때
		DELETE 지울때 할때
		PUT 업데이트 할때
		
		주소로 보낸다(a href="request-service") -> get방식
		
		분기처리(service)가 해주고있다.
		분기처리란? if문같이 조건문을해서 어떤건 어디로 ~ 이런걸처리하는걸 말한다.
		Service는 doget으로 보낼건지 dopost로 보낼건지 알아서 분기처리를해준다.
		
		----------------------------------------
		index.html
		<form> : 어디로보낼거다 ( 위치 )?
		action : 위치를 담는 속성값 
		method : 보낼때 메소드방식
		submit : 일반태그와 다르게 form태그로 완료찍겠다.
		           ( 전송해줘 ! ) 버튼에대한 완료처리는 submit으로한다.
		----------------------------------------
		<form>태그 는 어떤걸 나타내는건가?
		웹브라우저를 서버로 보내기위해서 감싸져 있는 태그이다.
		  action = request-service 
		  method = 없으면 기본 get , post면쓴다.
		  <input type="submit" value="GET방식 요청 전송" 
		</form>
		
		
		chap02
		
		QueryString?
		http://localhost:8888/chap03/index.html?userid=user01
		                                                 ?부터 쿼리스트링이다.
		?표뒤에 붙은 키값 을 의미한다
		userid : key값
		user01 : value값
		
		--------------------------------------------------------------------------------
		index.html
		<form> 안에 input
		  <input = type="text" name="name" 
		   name속성 : key값 input전체 value값 (Map)형태로 전달시킨다. key,value로  
		   input전체 : value값
		
		</form>
		--------------------------------------------------------------------------------
		* HttpServletRequest는 요청시 전달한 값을 getParameter()메소드를 이용해 추출.
		  name속성의 값을 "문자열"형태로 전달해야한다.
		* 화면에서 전달한 form내의 모든 input태그의 값을 HashMAp으로 관리하고있다.
		-연습-
		String name = request.getParameter("name")
		System.out.println(name);
		html에 쓴 name키값을 가져온다.
		
		Get방식 특징 서버를 실행 시키면 주소에 
		http://localhost:8080/chap03/queryString?name=홍길동
		이렇게 값을 다 나타나게 해준다,
		단점 : 보안취약, 로그인 이런건 get방식 X 
		
		
		String name = request.getParameter("name");
		
		숫자로 입력받기 위해선 int 로 변환시켜야한다.
		int age = Integer.parseInt(request.getParameter("age"));
		날짜를 받아오기위해서 ValueOf를 이용했다.
		java.sql.Date birthday = java.sql.Date.valueOf(request.getParameter("birthday"));
		
		String gender = request.getParameter("gender");
		
		String national = request.getParameter("national");
		
		여러개의 값을 가져올땐 배열을 이용한다. 그리고 getParameterValues를 사용한다.
		String[] hobbies = request.getParameterValues("hobbies");
		for(String hobby : hobbies) {
		    System.out.println(hobby);
		}
		
		-------------------------------------------- POST 방식 ---------------------------------------------
		POST 방식은 GET방식과 달리 한글에 대한 내용을 인코딩 해줘야한다.
		request.setCharacterEncoding("UTF-8");
		
		-------------------------------------------- forward ---------------------------------------------
		forward 방식
		기존 A라는 서블릿을 B라는 서블릿으로 이동시킬때
		기존 그대로 유지시켜서 보는 방식요청을 forward방식 이라고 한다.
		A에서 B로 이동해도 주소값은 변하지않는다 ( 기존 A 주소 )
		
		Redirect(리다이렉트) ( A에서 B 이동하면 주소변한다. )
		
		 
		Servlet이 하는 역할은 3가지  (Controller)같은역할이다.
		1. 요청 정보 받기
		2. 비즈니스 로직 처리 (어느 서비스 요청해서 이결과에 대한 내용을 처리할건지 )
		3. 응답 내보내기
		
		--ReceiveInformationServlet 서블릿 페이지 - 
		
		request.setAttribute("uesr01", userId);
		보낼때
		
		RequestDispatcher rd = request.getRequestDispatcher("print")
		rd.forward(request, respone);
		보낼때 객체
		
		print 서블릿으로 요청하겠다.
		
		-- print 서블릿 --
		String userId = (String) request.getAttribute("userId");
		값 : user01
		String userId = (String) request.getAttribute("password");
		값 : null
		   위 에서 setAttribute로 user01만 넘겨줬다. password는 넘겨주지 않았다.
		   전페이지에서 setAttribute를 해주면 다음 페이지에서도 기존껄 그대로 사용하지만
		   setAttribute를 하지않았던건 null값으로 나오며,  보여주고싶으면 
		   getParameter로 가져오면 된다.
		String password2 = request.getParameter("password");
		값 : pass01
		
		
		
		정리
		(view)역할             (Controller)역할                       
		index.html ---->         서블릿A           ----------------------------------------> 서블릿B
		                request.getParameter("userId");
		            
		                             서블릿A           ----------------------------------------> 서블릿B
		        request.setAttribute("userId", userId)
		        RequestDispatcher rd = request.getRequestDispatcher("서블릿B")
		        rd.foward(request.respoonse);     
									request.getAttribute("userId") 보낸서블릿A소환
		                                                                                    request.getParameter("pwd")안보낸서블릿B소환
		
		
		
		



	 * */
}
